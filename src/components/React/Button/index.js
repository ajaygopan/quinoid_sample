/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import React from 'react';
import {View, TouchableHighlight, Text} from 'react-native';

import {AppStyles, AppColors} from '../../../themes';
import styles from './styles';

export default props => {
  return (
    <View>
      <TouchableHighlight
        activeOpacity={1}
        underlayColor={props.underlayColor}
        disabled={props.disabled}
        style={
          props.disabled
            ? [styles.buttonTouchDisabled]
            : [
                {
                  backgroundColor: props.buttonColor
                    ? props.buttonColor
                    : 'transparent',
                  borderColor: props.borderColor
                    ? props.borderColor
                    : AppColors.brand.primary,
                },
                styles.buttonTouch,
              ]
        }
        {...props}
        onPress={props.onPress.bind(this)}>
        <Text
          style={[
            styles.buttonText,
            {
              color:'#FFF',
            },
          ]}>
          {props.buttonText}
        </Text>
      </TouchableHighlight>
    </View>
  );
};
