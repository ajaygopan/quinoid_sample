/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  loadingMainView: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: '#FFFFFF50',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loadingContainerView: {
    borderRadius: 100,
    padding: 10,
  },
});
