/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import React from 'react';
import {View, ActivityIndicator} from 'react-native';

import {AppColors} from '../../../themes';
import styles from './styles';

export default ({color}) => {
  return (
    <View style={styles.loadingMainView}>
      <View style={styles.loadingContainerView}>
        <ActivityIndicator
          size="large"
          color={color ? color : AppColors.bgRed}
        />
      </View>
    </View>
  );
};
