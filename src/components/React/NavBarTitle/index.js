/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {AppStyles} from '../../../themes';
import {NavigationContainer, useNavigation} from '@react-navigation/native';

// Redux Modules
import {useDispatch, useSelector} from 'react-redux';

export default props => {
  const dispatch = useDispatch();
  let navBarTitle = props.title
  console.log({props})
  const navigation = useNavigation();

  return (
    <View
      style={[
        AppStyles.navbarView,
        {...(props.titlePosition ? {justifyContent: 'center',left:-16} : {justifyContent: 'center'})},
      ]}>
      {props.backIcon ? 
        <TouchableOpacity
          style={AppStyles.navbarBackIconView}
          onPress={() => {
            navigation.goBack(null);
          }}>
          <Image style={[AppStyles.navbarIcon]} resizeMode={'contain'}
          source={require('../../../images/back.png')} />
        </TouchableOpacity>
       : null}
      <Text style={{fontSize:props.fontSize?props.fontSize:16}}
        numberOfLines={1}>
        {navBarTitle}
      </Text>
      {props.rightIcon ? (
        <TouchableOpacity
          style={AppStyles.navbarIconView}
          onPress={() => {
            props.searchIcon
              ? dispatch(searchInventory(true))
              : props.navigation.navigate(props.navigationScene);
          }}>
            <Image style={[AppStyles.navbarIcon]} source={props.rightIcon} />
        </TouchableOpacity>
      ) : null}
    </View>
  );
};
