/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import React, {useState, useEffect} from 'react';
import {View, Image, Text} from 'react-native';
import Overlay from 'react-native-modal-overlay';
import NetInfo from '@react-native-community/netinfo';

import {AppResources} from '../../../config';
import styles from './styles';

export default () => {
  const [netInfoVisible, setNetInfoVisible] = useState(false);
  useEffect(() => {
    NetInfo.addEventListener(state => {
      if (state.type == 'none') {
        setNetInfoVisible(true);
      } else if (state.isConnected == true && state.type != 'none') {
        setNetInfoVisible(false);
      }
    });
  });

  return (
    <Overlay
      visible={netInfoVisible} //netInfoVisible
      closeOnTouchOutside
      animationType="zoomIn"
      containerStyle={styles.netInfoContainer}
      childrenWrapperStyle={styles.netInfoSubContainer}
      animationDuration={100}>
      <View style={styles.netInfoItems}>
        <Image style={styles.netInfoImage} source={AppResources.netInfoIcon} />
        <Text style={[styles.netInfoTitle]}>
          {"Whoops"}
        </Text>
        <Text style={[styles.netInfoSubTitle]}>
          {"Slow or no internet connection. \n Please check your internet settings"}
        </Text>
      </View>
    </Overlay>
  );
};
