/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import {StyleSheet, Dimensions} from 'react-native';
import {AppColors} from '../../../themes';

export default StyleSheet.create({
  netInfoContainer: {
    flex: 1,
    padding: 0,
    margin: 0,
    //paddingTop: 65,
    backgroundColor: '#FFFFFF01',
  },
  netInfoSubContainer: {
    flex: 1,
    backgroundColor: '#00000099',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  netInfoItems: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 20,
  },
  netInfoImage: {
    width: Dimensions.get('screen').width / 2,
    height: Dimensions.get('screen').width / 2,
  },

  netInfoTitle: {
    textAlign: 'center',
    color: '#000',
    fontSize: 28,
  },
  netInfoSubTitle: {
    marginTop: 10,
    textAlign: 'center',
    color: '#000',
    fontSize: 18,
  },
});
