
/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

const app = {
  
  background: '#FAFCFF',
  yellow: '#FFBF67',
  bgGreen: '#009971',
  bgRed:'red',
  white:'#fff',
  gray:'gray',
  lightgray:'lightgray'
};

export default {
  ...app,
};
