
/**
* Quinoid Sample Application
*
* @author      AJAY G
* @license     -
*
**/

'use strict';

import AppColors from './colors';
import AppStyles from './styles';

export { AppColors,  AppStyles };
