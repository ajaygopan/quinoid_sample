/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';
import {Platform, Dimensions} from 'react-native';

import Colors from './colors';

export default {
  safeAreaView: {
    flex: 1,
  },
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.bgPrimary,
  },
  navbarIcon:{
    width: 24,
    height: 24,
  },
  navbarIconView: {
    position: 'absolute',
    right: 20,
  },
  navbarBackIconView: {
    position: 'absolute',
    left: 10,
  },
  navbar: {
    backgroundColor: '#fff',
    borderBottomWidth: 0,
    ...Platform.select({
      ios: {
        shadowRadius: 0,
        shadowOffset: {
          height: 0,
          width: 0,
        },
        shadowColor: '#000',
        shadowOpacity: 0.2,
      },
      android: {
        elevation: 0
      },
    }),
  },
  navbarView: {
    width: Dimensions.get('window').width,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

  },
};
