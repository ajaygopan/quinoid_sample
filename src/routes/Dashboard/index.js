/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import React, { useState, useEffect, useCallback } from 'react';
import {
  StatusBar,
  View,
  Text,
  Image,
  LogBox,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  SafeAreaView,
  Button,
  RefreshControl,
} from 'react-native';

// Redux Modules
import { useDispatch, useSelector } from 'react-redux';
import { initDashboard } from '../../stores/dashboard';

import LoadingFull from '../../components/React/LoadingFull';

import {
  AppConstants
} from '../../config';
import { AppColors, AppStyles } from '../../themes';
import styles from './styles';

function renderLoading(loading) {
  if (loading) {
    return <LoadingFull />;
  } else {
    return null;
  }
}

export default ({ navigation }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const dashboardObj = useSelector(state => state.dashboard);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    initDashboardData();
  }, [dispatch]);

  const initDashboardData = () => {

    setLoading(true);
    dispatch(initDashboard())
      .catch(err => console.log(err))
      .finally(response => {
        setLoading(false);
      });
  };

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);


  const onRefresh = useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      initDashboardData
      setRefreshing(false);
    }, 2000);
  }, []);

  return (
    <SafeAreaView style={AppStyles.safeAreaView}>

      <View style={styles.sceneView}>
        <TouchableOpacity 
        style={styles.buttonContainer}
        onPress={()=>navigation.navigate(AppConstants.scene.taskOneDashBoard)}
        >
          <Text style={styles.textStyle}>{"TASK 1"}</Text>
        </TouchableOpacity>

        <TouchableOpacity 
        style={[styles.buttonContainer,{backgroundColor:AppColors.gray}]}
        onPress={()=>navigation.navigate(AppConstants.scene.taskTwoDashBoard)}>
          <Text style={styles.textStyle}>{"TASK 2 - Clean Ocean"}</Text>
        </TouchableOpacity>
        {renderLoading(loading)}

      </View>



    </SafeAreaView>
  );
};
