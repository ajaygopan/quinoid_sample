/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import {StyleSheet, Dimensions, Platform} from 'react-native';
import {AppStyles, AppColors} from '../../themes';

export default StyleSheet.create({
  sceneView: {
    paddingTop: 50,
    flex: 1,
    backgroundColor: '#F9F9F9',
    // justifyContent:'center',
    alignItems:'center'
  },
  textStyle:{
    color:AppColors.white,
    fontSize:16,
    fontWeight:'bold'
  },
  boldText: {
    color:'#000',
    fontSize: 14,
    fontWeight: 'bold',
    top:10
  },
  text:{
    color:'#000',
    fontSize: 14,
    top:10
  }

});
