/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import React, { useState, useEffect, useCallback } from 'react';
import {
  StatusBar,
  View,
  Text,
  Image,
  LogBox,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  SafeAreaView,
  Button,
  RefreshControl,
} from 'react-native';

// Redux Modules
import { useDispatch, useSelector } from 'react-redux';
import { initDashboard } from '../../stores/dashboard';

import LoadingFull from '../../components/React/LoadingFull';

import {
  AppConstants
} from '../../config';
import { AppColors, AppStyles } from '../../themes';
import styles from './styles';

function renderLoading(loading) {
  if (loading) {
    return <LoadingFull />;
  } else {
    return null;
  }
}

export default ({ navigation, route }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const dashboardObj = useSelector(state => state.dashboard);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);


  return (
    <SafeAreaView style={AppStyles.safeAreaView}>

      <View style={styles.sceneView}>
        <Image
          source={{ uri: dashboardObj?.dashboardData[route?.params?.index]?.user?.profile_image?.large }}
          style={{ width: 100, height: 100, borderRadius: 1000 }} />

        <View style={{ width: '96%', marginLeft: '2%', top: 20 }}>
          <Text style={[styles.text, { color: AppColors.gray }]}>{'Name :'}</Text>
          <Text style={styles.boldText}>{dashboardObj?.dashboardData[route?.params?.index]?.user?.first_name} {dashboardObj?.dashboardData[route?.params?.index]?.user?.last_name}</Text>
        </View>


        <View style={{ width: '96%', marginLeft: '2%', marginTop: '10%' }}>
          <Text style={[styles.text, { color: AppColors.gray }]}>{'Location :'}</Text>
          <Text style={styles.boldText}>{dashboardObj?.dashboardData[route?.params?.index]?.user?.location}</Text>
        </View>

        <View style={{ width: '96%', marginLeft: '2%', marginTop: '5%' }}>
          <Text style={[styles.text, { color: AppColors.gray }]}>{'Bio :'}</Text>
          <Text style={[styles.text, { top: 20 }]}>{dashboardObj?.dashboardData[route?.params?.index]?.user?.bio}</Text>
        </View>

      </View>
      {renderLoading(loading)}
    </SafeAreaView>
  );
};
