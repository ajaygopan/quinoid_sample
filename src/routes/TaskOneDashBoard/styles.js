/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import { StyleSheet, Dimensions, Platform } from 'react-native';
import { AppStyles, AppColors } from '../../themes';
import colors from '../../themes/colors';

export default StyleSheet.create({
  sceneView: {
    paddingTop: 5,
    flex: 1,
    backgroundColor: '#F9F9F9',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonContainer: {
    width: '80%',
    height: 100,
    backgroundColor: AppColors.bgGreen,
    margin: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle: {
    color: AppColors.white,
    fontSize: 16,
    fontWeight: 'bold'
  },
  contentContainetr: {
    width: '100%',
    height: 250,
    backgroundColor: colors.white,
    elevation: 5,
    borderRadius: 1,
    borderBottomWidth: 2,
    borderBottomColor: AppColors.lightgray,
    justifyContent: 'space-around'
  },
  imgHeader: {
    width: '80%',
    height: 120,
    marginLeft: '10%',
    top: 20,
    borderRadius: 10
  },
  text: {
    color: '#000',
    fontSize: 14,
    padding:5,
    top:5
  },
  boldText: {
    color:'brown',
    fontSize: 14,
    fontWeight: 'bold'
  }

});
