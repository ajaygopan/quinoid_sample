/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import React, { useState, useEffect, useCallback } from 'react';
import {
  StatusBar,
  View,
  Text,
  Image,
  LogBox,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  SafeAreaView,
  Button,
  RefreshControl,
  Dimensions,
} from 'react-native';

// Redux Modules
import { useDispatch, useSelector } from 'react-redux';
import { initDashboard } from '../../stores/dashboard';

import LoadingFull from '../../components/React/LoadingFull';

import {
  AppConstants
} from '../../config';
import { AppColors, AppStyles } from '../../themes';
import styles from './styles';

function renderLoading(loading) {
  if (loading) {
    return <LoadingFull />;
  } else {
    return null;
  }
}

export default ({ navigation }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const dashboardObj = useSelector(state => state.dashboard);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    initDashboardData();
  }, [dispatch]);

  const initDashboardData = () => {
    setLoading(true);
    dispatch(initDashboard())
      .catch(err => console.log(err))
      .finally(response => {
        setLoading(false);
      });
  };

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);


  const onRefresh = useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      initDashboardData
      setRefreshing(false);
    }, 2000);
  }, []);

  return (
    <SafeAreaView style={AppStyles.safeAreaView}>
      <View style={AppStyles.container}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
              colors={['#009971']}
            />
          }>
          {console.log({ dashboardObj })}
          {dashboardObj && dashboardObj?.dashboardData.map((data, i) => {
            return (
              <View style={styles.contentContainetr}>
                <TouchableOpacity style={styles.imgHeader}
                onPress={()=>navigation.navigate(AppConstants.scene.imgViewer,{index:i})}>
                  <Image
                    source={{ uri: data?.urls?.full }}
                    style={{ width: '100%', height: '100%' }} />
                </TouchableOpacity>
                <View style={{ padding: 5, left: 5 }}>
                  <Text
                    numberOfLines={2}
                    style={styles.text}>
                    {data?.description}
                  </Text>
                </View>
                <TouchableOpacity style={{ flexDirection: 'row', left: 10, alignItems: 'center' }}
                onPress={()=>navigation.navigate(AppConstants.scene.userInfo,{index:i})}>
                  <Image source={{ uri: data?.user?.profile_image?.large }}
                    style={{ width: 35, height: 35, margin: 5 }} />
                  <Text style={styles.boldText}>
                    {data?.user?.first_name + " " + data?.user?.last_name}
                  </Text>
                </TouchableOpacity>
              </View>
            )
          })}
        </ScrollView>
      </View>
      {renderLoading(loading)}
    </SafeAreaView>
  );
};
