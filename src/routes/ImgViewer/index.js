/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import React, { useState, useEffect, useCallback } from 'react';
import {
  StatusBar,
  View,
  Text,
  Image,
  LogBox,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  SafeAreaView,
  Button,
  RefreshControl,
} from 'react-native';

// Redux Modules
import { useDispatch, useSelector } from 'react-redux';
import { initDashboard } from '../../stores/dashboard';

import LoadingFull from '../../components/React/LoadingFull';

import {
  AppConstants
} from '../../config';
import { AppColors, AppStyles } from '../../themes';
import styles from './styles';

function renderLoading(loading) {
  if (loading) {
    return <LoadingFull />;
  } else {
    return null;
  }
}

export default ({ navigation,route }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const dashboardObj = useSelector(state => state.dashboard);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    console.log(dashboardObj?.dashboardData[route?.params?.index]?.description)
  }, [dispatch]);

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);

  return (
    <SafeAreaView style={AppStyles.safeAreaView}>

      <View style={styles.sceneView}>
        <View style={{width:'100%',height:'60%',backgroundColor:'rgba(255,255,255,0.1)',paddingTop:30,paddingBottom:30}}>
        <Image source={{uri:dashboardObj?.dashboardData[route?.params?.index]?.urls?.full}}
       style={{width:'100%',height:'100%'}}
       resizeMode={'contain'}/>
          </View>
          <View style={{width:'100%',height:100,backgroundColor:'rgba(255,255,255,0.1)',}}>
       <Text style={styles.text}>{dashboardObj?.dashboardData[route?.params?.index]?.description}</Text>
</View>
     
        {renderLoading(loading)}

      </View>



    </SafeAreaView>
  );
};
