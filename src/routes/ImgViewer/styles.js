/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import {StyleSheet, Dimensions, Platform} from 'react-native';
import {AppStyles, AppColors} from '../../themes';

export default StyleSheet.create({
  sceneView: {
    paddingTop: 5,
    flex: 1,
    backgroundColor: '#000',
    justifyContent:'center',
    alignItems:'center'
  },
  imgContainer: {
    backgroundColor: 'gray',
    justifyContent:'center',
    alignItems:'center'
  },
  text: {
    color: '#fff',
    fontSize: 14,
    padding:10
  },
  

});
