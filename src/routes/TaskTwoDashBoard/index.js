// /**
//  * Quinoid Sample Application
//  *
//  * @author      AJAY G
//  * @license     -
//  *
//  **/

// 'use strict';

// import React, { useState, useEffect, useCallback } from 'react';
// import {
//   StatusBar,
//   View,
//   Text,
//   Image,
//   LogBox,
//   ScrollView,
//   TouchableHighlight,
//   TouchableOpacity,
//   SafeAreaView,
//   Button,
//   RefreshControl,
// } from 'react-native';

// // Redux Modules
// import { useDispatch, useSelector } from 'react-redux';
// import { initDashboard } from '../../stores/dashboard';

// import LoadingFull from '../../components/React/LoadingFull';

// import {
//   AppConstants
// } from '../../config';
// import { AppColors, AppStyles } from '../../themes';
// import styles from './styles';

// function renderLoading(loading) {
//   if (loading) {
//     return <LoadingFull />;
//   } else {
//     return null;
//   }
// }

// export default ({ navigation }) => {
//   const dispatch = useDispatch();
//   const [loading, setLoading] = useState(false);
//   const dashboardObj = useSelector(state => state.dashboard);
//   const [refreshing, setRefreshing] = useState(false);

//   useEffect(() => {
//     initDashboardData();
//   }, [dispatch]);

//   const initDashboardData = () => {

//     setLoading(true);
//     dispatch(initDashboard())
//       .catch(err => console.log(err))
//       .finally(response => {
//         setLoading(false);
//       });
//   };

//   useEffect(() => {
//     LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
//   }, []);


//   const onRefresh = useCallback(() => {
//     setRefreshing(true);
//     setTimeout(() => {
//       initDashboardData
//       setRefreshing(false);
//     }, 2000);
//   }, []);

//   return (
//     <SafeAreaView style={AppStyles.safeAreaView}>

//      <Text>Loading...</Text>


//     </SafeAreaView>
//   );
// };

import React, { useContext, useEffect, useState } from 'react';
import {
  Image, View, Text, Dimensions,
  ImageBackground
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from './styles';
// import {Ctx} from '../game-controller/context';

export default function Mario() {
  // const [controller] = useContext(Ctx);
  const [controller, setController] = useState(0);
  const [direction, setDirection] = useState('180deg');
  const [position, setPosition] = useState({ x: 335 / 2, y:335 / 2 });
  const joystick = controller;
  const boundary = 335;
  // const standing = require('../../images/sea.jpeg');
  // const walking = require('../../images/back.png');
  const [grid, setGrid] = useState(require('../../images/grid.jpeg'));
  const [walking, setwalking] = useState(require('../../images/n.png'));
  const [pos, setPos] = useState('n');

  // var grid = require('../../images/grid.jpeg');
  const duration = 100;
  const speed = 3;
  useEffect(() => {
    if (controller !== 0) {
      setDirection(controller > 0 ? '180deg' : '360deg');
    }
    setPosition({
      x: controller > 0 ?
        position.x <= boundary ?
          position.x + speed :
          position.x :
        position.x >= 0 ?
          position.x - speed :
          position.x,
      y: 0
    })

  }, [duration]);

  const moveIcon = () =>{
    // alert(pos)
    pos == 'e' ?
    setPosition({
      x: controller > 0 ?
        position.x <= boundary ?
          position.x + speed :
          position.x :
        position.x >= 0 ?
          position.x + speed :
          position.x,
      y: 0
    })
    :
    pos == 'w' ?
    setPosition({
      x: controller > 0 ?
        position.x <= boundary ?
          position.x + speed :
          position.x :
        position.x >= 0 ?
          position.x - speed :
          position.x,
      y: 0
    })
    :
    pos == 'n' ?
    setPosition({
      x: 0,
      y: controller > 0 ?
      position.y <= boundary ?
        position.y + speed :
        position.y :
      position.y >= 0 ?
        position.y + speed :
        position.y,
    })
    :
    pos == 's' ?
    setPosition({
      x: 0,

      y: controller > 0 ?
      position.x <= boundary ?
        position.x + speed :
        position.x :
      position.x >= 0 ?
        position.x - speed :
        position.x,
    })
    :
    null

    
  }
  const directionSwitch = (dir) =>{
    dir == 'up'?
    setwalking(require('../../images/n.png'),setPos('n'))
    :
    dir == 'down'?
    setwalking(require('../../images/s.png'),setPos('s'))
    :
    dir == 'right'?
    setwalking(require('../../images/e.png'),setPos('e'))
    :
    dir == 'left'?
    setwalking(require('../../images/w.png'),setPos('w'))
    :
    null

    // setGrid(require('../../images/netInfo_icon.png'))
  }
  

  return (
    <View >
      <ImageBackground source={grid} style={{
        backgroundColor: 'gray',
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height * 0.5
      }}>
        <Image
          source={
            duration > 0 ?
              position.x >= boundary || position.x <= 0 ?
                walking : walking
              : walking}
          style={{
            position: 'absolute',
            bottom: 118,
            left: position.x,
            bottom:position.y,
            width:80,
            height:80
            // transform: [
            //   { rotateY: direction }

            // ]
          }
          }
          resizeMode={'contain'}
        />
      </ImageBackground>


      {/* <Text style={{ color: '#fff', fontSize: 25 }}
        onPress={() =>
          setPosition({
            x: controller > 0 ?
              position.x <= boundary ?
                position.x + speed :
                position.x :
              position.x >= 0 ?
                position.x - speed :
                position.x,
            y: 0
          })
        }>AJAY</Text> */}


      <View style={{ flexDirection: 'row',justifyContent:'center',alignItems:'center',top:Dimensions.get('screen').height * 0.05 }}>
        <View>
          <TouchableOpacity style={styles.button}
          onPress={()=>directionSwitch("left")}>
            <Text style={{ fontSize: 16, color: '#fff', fontWeight: 'bold' }}>LEFT</Text>
          </TouchableOpacity>
        </View>

        <View>
          <TouchableOpacity style={styles.button}
          onPress={()=>directionSwitch("up")}>
            <Text style={{ fontSize: 16, color: '#fff', fontWeight: 'bold' }}>UP</Text>
          </TouchableOpacity>

          <TouchableOpacity style={[styles.button,{
            backgroundColor:'red'
          }]}
          onPress={()=>moveIcon()}>
            <Text style={{ fontSize: 16, color: '#fff', fontWeight: 'bold' }}>{"MOVE"}</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button}
          onPress={()=>directionSwitch("down")}>
            <Text style={{ fontSize: 16, color: '#fff', fontWeight: 'bold' }}>DOWN</Text>
          </TouchableOpacity>
        </View>

        <View>
          <TouchableOpacity style={styles.button}
          onPress={()=>directionSwitch("right")}>
            <Text style={{ fontSize: 16, color: '#fff', fontWeight: 'bold' }}>RIGHT</Text>
          </TouchableOpacity>
        </View>

      </View>

    </View>

  );

}
