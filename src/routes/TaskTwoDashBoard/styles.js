/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import {StyleSheet, Dimensions, Platform} from 'react-native';
import {AppStyles, AppColors} from '../../themes';

export default StyleSheet.create({
  sceneView: {
    paddingTop: 5,
    flex: 1,
    backgroundColor: '#F9F9F9',
    justifyContent:'center',
    alignItems:'center'
  },
  buttonContainer:{
      width: '80%',
      height: 100,
      backgroundColor: AppColors.bgGreen,
      margin:20,
      justifyContent:'center',
      alignItems:'center'
  },
  textStyle:{
    color:AppColors.white,
    fontSize:16,
    fontWeight:'bold'
  },
  button:{
    backgroundColor: 'steelblue',
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth:1,
    borderColor:'#fff'
  }

});
