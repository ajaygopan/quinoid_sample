/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

import {AppConstants} from '../../config';

//----------------------- Populate Dashboard Data ------------------------
export const dashboardData = () => {
  return new Promise((resolve, reject) => {
    axios
      .get('https://api.unsplash.com/photos/?client_id=8634366274bd23efb9b023fb9b2c6502e67f7dd5d6a7962b3b49fbee170940f8')
      .then(function (response) {
        console.log('DASHBOARD RESPONSE DATA',response)
        if (
          response &&
          response.status === 200 &&
          response?.data
        ) {
          resolve(response.data);
        } else if (response && response.data && response.data.message) {
          reject({msg: response.data.message});
        } else {
          reject({msg: "Something Went Wrong, Please ReTry !!!"});
        }
      })
      .catch(function (error) {
        reject({msg: "Something Went Wrong, Please ReTry !!!."});
      });
  });
};
