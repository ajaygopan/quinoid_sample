/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

export * from './actions';
export {default as dashboard} from './reducers';

export const SET_DASHBOARD_DATA = 'SET_DASHBOARD_DATA';
