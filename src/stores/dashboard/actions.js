/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import {SET_DASHBOARD_DATA} from '.';
import {dashboardData} from './modules';

//----------------------- Init Dashboard Data ------------------------
export const initDashboard = () => {
  return (dispatch) => {
    return dashboardData().then(response => {
      dispatch({
        type: SET_DASHBOARD_DATA,
        payload: response,
      });
    });
  };
};
