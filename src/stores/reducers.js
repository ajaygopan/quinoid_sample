/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import {combineReducers} from 'redux';

import {dashboard} from './dashboard';

export default combineReducers({
  dashboard
});
