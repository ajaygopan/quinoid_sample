/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import React from 'react';
import {Image, View, Text} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector, useDispatch} from 'react-redux';
import {AppConstants, AppResources} from '../config';
import {AppStyles} from '../themes';

// Pages list
import Dashboard from '../routes/Dashboard';
import TaskOneDashboard from '../routes/TaskOneDashBoard';
import TaskTwoDashboard from '../routes/TaskTwoDashBoard';
import ImgViewer from '../routes/ImgViewer';
import UserInfo from '../routes/UserInfo';
import NavBarTitle from '../components/React/NavBarTitle';


const mainStack = (props) => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={Dashboard}>
        <Stack.Screen
          name={AppConstants.scene.dashBoard}
          component={Dashboard}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name={AppConstants.scene.taskOneDashBoard}
          component={TaskOneDashboard}
          options={{
            headerTitleAlign: 'center',
            headerStyle: AppStyles.navbar,
            headerLeft:false,
            headerTitle: () => (
              <NavBarTitle
                title={"PHOTOS"}
                titlePosition={'center'}
                navigation={props.navigation}
                fontSize={18}
                // backIcon={false}
                //navigationScene={AppConstants.scene.trackDriver}
                {...props}
              />
            ),
          }}
        />
         <Stack.Screen
          name={AppConstants.scene.taskTwoDashBoard}
          component={TaskTwoDashboard}
          options={{
            headerTitleAlign: 'center',
            headerStyle: AppStyles.navbar,
            headerLeft:false,

            headerTitle: () => (
              <NavBarTitle
                title={""}
                titlePosition={'center'}
                navigation={props.navigation}
                backIcon={true}
                //navigationScene={AppConstants.scene.trackDriver}
                {...props}
              />
            ),
          }}
        />
         <Stack.Screen
          name={AppConstants.scene.imgViewer}
          component={ImgViewer}
          options={{
            headerTitleAlign: 'center',
            headerStyle: AppStyles.navbar,
            headerLeft:false,
            headerTitle: () => (
              <NavBarTitle
                title={""}
                titlePosition={'center'}
                navigation={props.navigation}
                backIcon={true}
                //navigationScene={AppConstants.scene.trackDriver}
                {...props}
              />
            ),
          }}  
        />
        <Stack.Screen
          name={AppConstants.scene.userInfo}
          component={UserInfo}
          options={{
            headerTitleAlign: 'center',
            headerStyle: AppStyles.navbar,
            headerLeft:false,
            headerTitle: () => (
              <NavBarTitle
                title={"PROFILE"}
                titlePosition={'center'}
                navigation={props.navigation}
                backIcon={true}
                //navigationScene={AppConstants.scene.trackDriver}
                {...props}
              />
            ),
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default function RootNavigation() {
  const dispatch = useDispatch();
  return mainStack(dispatch);
}

const Stack = createStackNavigator();
