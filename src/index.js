/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import React, {useEffect} from 'react';
import {Provider as StoreProvider, useDispatch} from 'react-redux'; 
import {PersistGate} from 'redux-persist/integration/react';

import {store, persistor} from './stores';
import RootNavigation from './navigations';
import NetInfo from './components/Lightboxes/NetInfoBox';


export default () => {
  const ReInitialize = () => {
    const dispatch = useDispatch();

    useEffect(() => {
     
    }, []);

    return (
      <PersistGate loading={null} persistor={persistor}>
        <RootNavigation />
        <NetInfo />
      </PersistGate>
    );
  };

  return (
    <StoreProvider store={store}>
      <ReInitialize />
    </StoreProvider>
  );
};
