/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/
'use strict';

const resources = {
  netInfoIcon: require('../images/netInfo_icon.png')
};

export default resources;
