/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

//staging URLs
const baseURL = 'https://waicart.com/api/V1/?';

const scene = {
  dashBoard: 'dashboard',
  taskOneDashBoard: 'taskOneDashBoard',
  taskTwoDashBoard: 'taskTwoDashBoard',
  imgViewer: 'imgViewer',
  userInfo: 'userInfo',
};


const constants = {
  baseURL,
  scene,
};

export default constants;
