/**
 * Quinoid Sample Application
 *
 * @author      AJAY G
 * @license     -
 *
 **/

'use strict';

import AppConstants from './constants';
import AppResources from './resources';

export { AppConstants, AppResources};
